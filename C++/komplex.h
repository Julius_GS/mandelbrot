#pragma once
#include <iostream>
#include <cmath>
using namespace std;

//Klasse zum Ausdrücken von komplexen Zahlen in C++
class komplex{
private:
	//Real- und Imaginärteil als Attribute der Klasse
	double re, im;
public:
	//Standardkonstruktor
	komplex(void){
		im = 0;
		re = 0;
	}

	//Konstruktor für Real- und Imaginärteil
	komplex(double r, double i){
		im = i;
		re = r;
	}

	//Konstruktor ausschließlich für Realteil
	komplex(double r){
		re = r;
		im = 0;
	}

	//Copy-Konstruktor
//komplex(komplex other){
//	re = other.re;
//	im = other.im;
//}

	//Zuweisungsoperator überladen
	void operator=(const komplex& other){
		re = other.re;
		im = other.im;
	}

	//Methode zur Komplexen Konjugation
	komplex con(){
		komplex res(0,0);
		res.re = re;
		res.im = -im;
		return res;
	}

	//Methode zum "Callen" des Realteils außerhalb der Klasse
	double Re(){
		return re;
	}

	//Methode zum "Callen" des Imaginärteils außerhalb der Klasse 
	double Im(){
		return im;
	}

	//Methode um den Betrag/ die Norm der Zahl zu bekommen
	double betrag(){
		return sqrt(re*re + im*im);
	}

	//Additionsoperator überladen
	komplex operator+(const komplex& other){
		komplex res;
		res.re = re + other.re;
		res.im = im + other.im;
		return res;	
	}

	//+= Operator überladen.
	void operator+=(const komplex& other){
		*this = *this + other;
	}

	//Subtraktionsoperator überladen
	komplex operator-(const komplex& other){
		komplex res;
		res.re = re - other.re;
		res.im = im - other.im;
		return res;
	}

	//Multiplikationsoperator für zwei komplexe Zahlen überladen
	komplex operator*(const komplex& other){
		komplex res;
		res.re = 1.0*re*other.re - 1.0*im*other.im;
		res.im = 1.0*re*other.im + 1.0*other.re*im;
 		return res;
	}

	//Multiplikationsoperator für eine komplexe Zahl mit einer Zahl des Typs double überladen
	komplex operator*(const double other){
		komplex res;
		res.re = 1.0*re*other;
		res.im = 1.0*im*other;
		return res;
	}

	//Divisionsoperator für zwei komplexe Zahlen überladen
	komplex operator/(const komplex& other){
		komplex res;
		res.re = 1.0*(re*other.re + im*other.im) / (other.im*other.im + other.re*other.re);
		res.im = 1.0*(-re*other.im + im*other.re) / (other.im*other.im + other.re*other.re);
		return res;	
	}

	//Divisionsoperator für eine komplexe Zahl mit einer Zahl des Typs double überladen
	komplex operator/(const unsigned long int& other){
		komplex res;
		res.re = re / other;
		res.im = im / other;
		return res;
	}

	//Methode zur potenzierung einer komplexen Zahl
	komplex pow(unsigned int n){
		komplex res(1,0);
		for(int i=0; i<n; i++){
			res = res * *this;
		}	
		return res;
	}

	//Negativesvorzeichenoperator überladen
	komplex operator-(){
		komplex res;
		res.im = -im;
		res.re = -re;
		return res;
	}

	//Ausgabeoperator überladen
	friend ostream& operator<<(ostream& output, const komplex& ob){
		if(ob.re == 0){
			output << ob.im << "i";
		}else if(ob.im > 0){
			output << ob.re << "+" << ob.im << "i";
		}else if(ob.im < 0){
			output << ob.re << "-" << -ob.im << "i";
		}else{
			output << ob.re;
		}
		return output;
	}
};

komplex expo(komplex z){
	komplex res (1,0);
	unsigned long int fak=1;	
	for(int i=1; i<14; i++){
		res = res + z.pow(i) / fak;	
		fak *= i + 1;
	}
	return res;
}
