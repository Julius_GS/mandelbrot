#include "komplex.h"
#include <iostream>
#include <fstream>
using namespace std;

komplex Zuweisung(double re, double im);
unsigned int Iterationen(unsigned int ceiling, double maxval, komplex c);

int main()
{
    const float repoi = -0.77, ipoi = 0, intervall = 1.3;
	komplex start(repoi-intervall,ipoi-intervall); 				//Wert, der dem Pixel UNTEN links entspricht
	const unsigned int x = 1024, y = x; 	//Höhe/Breite des resultierenden Bildes
	const double step=2*intervall/x; 			//Schrittbreite, mit dem die Zahlen erhöht werden

//    komplex start(-2.6,-1.1);
//    const unsigned int x = 3840, y = 2160;
//    const double step = 2.2/y;

	const unsigned int Ceiling=1500; 	//Anzahl an iterationen, bei denen ein Wert als konstant gesehen wird
	const double Maxval=500; 			//Zahl, die |zn| erreichen soll
	ofstream output;

	output.open("values.txt", ofstream::app);

	komplex current=start;
	unsigned int dummy;
	for(unsigned int pixely = 1; pixely <= y; pixely++)
	{
		current = Zuweisung(start.Re(), current.Im());
		for(unsigned int pixelx = 1; pixelx < x; pixelx++)
		{
			dummy = Iterationen(Ceiling, Maxval, current);
			if(dummy != Ceiling)
			{
				output << dummy << ',';
			}else
			{
				output << 0 << ',';
			}
			current += Zuweisung(step,0);
		}
		dummy = Iterationen(Ceiling, Maxval, current);
        if(dummy != Ceiling)
        {
            output << dummy << endl;
        }else
        {
            output << 0 << endl;
        }
		current += Zuweisung(0,step);
	}

	output.close();
	return 0;
}

//Funktion, damit man komplexe Zahlen über den Zuweisungsoperator zuweisen kann
komplex Zuweisung(double re, double im)
{
	komplex res(re, im);
	return res;
}

/*Funktion, die die Anzahl der Iterationen an einem Punkt in der komplexen 
 * Ebene zurückgibt*/
unsigned int Iterationen(unsigned int ceiling, double maxval, komplex c)
{
	unsigned int res=0;
	komplex zn(0,0);
	while(zn.betrag() < maxval && res != ceiling){
		zn = zn*zn + c;
		res += 1;
        if(res > ceiling/4 && zn.betrag()<10){
            res = ceiling;
            break;
        }
	}
	return res;
}
