#!/usr/bin/env python3
from matplotlib import pyplot as plt
import numpy as np
import math
clipper=100
b=np.loadtxt(open("C++/values.txt", "rb"), delimiter=",")
print("groesster wert im array: ")
print(np.max(b))
a=np.clip(b,0,clipper)
plt.pcolormesh(a,cmap="inferno")
plt.clim(0,clipper)
# plt.colorbar()
plt.gca().set_aspect('equal', adjustable='box')
plt.gca().set_axis_off()
plt.savefig("images/output.jpg",bbox_inches='tight',pad_inches=0,dpi=600)
plt.show()
